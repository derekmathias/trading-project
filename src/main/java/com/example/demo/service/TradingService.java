package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Trading;
import com.example.demo.repository.TradingRepository;

@Service
public class TradingService {
	@Autowired
	private TradingRepository repository;
	
	public List<Trading> getAllTradings(){
		return repository.getAllTradings();
	}
	
	public Trading getTrading(int id) {
		return repository.getTradingById(id);
	}

	public Trading saveTrading(Trading Trading) {
		return repository.editTrading(Trading);
	}

	public Trading newTrading(Trading Trading) {
		return repository.addTrading(Trading);
	}

	public int deleteTrading(int id) {
		return repository.deleteTrading(id);
	}
}
